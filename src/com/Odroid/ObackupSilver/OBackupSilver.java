package com.Odroid.ObackupSilver;

import java.io.Serializable;

import android.content.Intent;
import android.os.Bundle;

import com.Odroid.ObackupCore.OBackupCore;
import com.Odroid.ObackupCore.OBackupCoreConstants;
import com.Odroid.ObackupCore.OBackupCoreService;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;

public class OBackupSilver extends OBackupCore {
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    AdManager.setTestDevices(new String[] { AdManager.TEST_EMULATOR, // emulator
    // "92C1B56F7C969AC6A37A2CCBED67F4E6", // My phone
        });
    // ((AdView)findViewById(R.id.ad)).setAdListener(
    // new AdListener() {
    // public void onFailedToReceiveAd ( AdView adView ) {
    // // this is what logs
    // Log.w ( "0BubbleAds", "AdMob failed to receive" );
    // }
    // public void onFailedToReceiveRefreshedAd ( AdView adView ) {
    // Log.w ( "0BubbleAds", "AdMob failed to refresh" );
    // }
    // public void onReceiveAd ( AdView adView ) {
    // Log.w ( "0BubbleAds", "AdMob received" );
    // }
    // public void onReceiveRefreshedAd ( AdView adView ) {
    // Log.w ( "0BubbleAds", "AdMob refreshed" );
    // }
    // }
    // );

    // ((AdView) findViewById(R.id.ad)).requestFreshAd();
  }

  protected void onResume() {
    super.onResume();
    ((AdView) findViewById(R.id.ad)).requestFreshAd();
  }

  protected int getLayoutMain() {
    return R.layout.main_silver;
  }

  protected void _startBackupService() {
    Intent i = new Intent(this, OBackupCoreService.class);
    i.putExtra(OBackupCoreConstants.BACKCLASS, (Serializable) OBackupSilver.class);
    startService(i);
  }

}
